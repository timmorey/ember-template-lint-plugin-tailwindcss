import assert from "assert";
import attributeToArray from "../../src/utils/attribute-to-array";

describe("attributeToArray()", function () {
  it("should split the class", function () {
    const input = "foo bar baz";
    const output = ["foo", "bar", "baz"];

    assert.deepStrictEqual(attributeToArray(input), output);
  });

  it("should cleanup", function () {
    const input = `  foo bar 
       baz   `;
    const output = ["foo", "bar", "baz"];

    assert.deepStrictEqual(attributeToArray(input), output);
  });
});
