import generateRuleTests from "ember-template-lint/lib/helpers/rule-test-harness";
import plugin from "../../../src/index";
import { stripIndent } from "common-tags";

generateRuleTests({
  name: "class-order",

  groupMethodBefore: beforeEach,
  groupingMethod: describe,
  testMethod: it,
  plugins: [plugin],
  config: {
    linebreakBetweenGroups: {
      indent: 7,
    },
  },

  good: [
    `<div class="bg-white p-1 rounded-lg"></div>`,
    stripIndent`
      <div
        class="gap-4 grid grid-cols-2
               lg:grid-cols-5 md:grid-cols-3 sm:grid-cols-3 xl:grid-cols-6"
      ></div>
    `,
  ],

  bad: [
    {
      template: `<div class="md:inline bg-white rounded-lg p-2 lg:my-2"></div>`,
      fixedTemplate: stripIndent`
        <div class="bg-white p-2 rounded-lg
                    lg:my-2 md:inline"></div>`,
      result: {
        message: stripIndent`
          HTML class attribute sorting is: 'md:inline bg-white rounded-lg p-2 lg:my-2', but should be: 'bg-white p-2 rounded-lg
                      lg:my-2 md:inline'`,
        line: 1,
        column: 5,
        isFixable: true,
        source: `<div class="md:inline bg-white rounded-lg p-2 lg:my-2"></div>`,
      },
    },
  ],
});
