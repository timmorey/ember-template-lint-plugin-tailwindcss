import generateRuleTests from "ember-template-lint/lib/helpers/rule-test-harness";
import plugin from "../../../src/index";

generateRuleTests({
  name: "class-order",

  groupMethodBefore: beforeEach,
  groupingMethod: describe,
  testMethod: it,
  plugins: [plugin],
  config: {
    disableForMustaches: false,
    warnForConcat: true,
  },

  good: [
    `<div class={{cat}}></div>`, // only mustache statement
    `<div class="{{dog}}"></div>`, // single mustache inside quotes
  ],

  bad: [
    {
      template: `<div class="foo-{{pig}}"></div>`,
      result: {
        message: `Dynamic segments in class names are not allowed: 'foo-{{pig}}'`,
        line: 1,
        column: 11,
        isFixable: false,
        source: `<div class="foo-{{pig}}"></div>`,
      },
    },
    {
      template: `<div class="foo {{goat}}-bar"></div>`,
      result: {
        message: `Dynamic segments in class names are not allowed: 'foo {{goat}}-bar'`,
        line: 1,
        column: 11,
        isFixable: false,
        source: `<div class="foo {{goat}}-bar"></div>`,
      },
    },
    {
      template: `<div class="{{hamster}}{{snake}}"></div>`,
      result: {
        message: `Dynamic segments in class names are not allowed: '{{hamster}}{{snake}}'`,
        line: 1,
        column: 11,
        isFixable: false,
        source: `<div class="{{hamster}}{{snake}}"></div>`,
      },
    },
  ],
});
