import generateRuleTests from "ember-template-lint/lib/helpers/rule-test-harness";
import plugin from "../../src/index";
import { stripIndent } from "common-tags";

generateRuleTests({
  name: "class-wrap",

  groupMethodBefore: beforeEach,
  groupingMethod: describe,
  testMethod: it,
  plugins: [plugin],
  config: {},

  good: [
    `<div class="foo1 foo2 foo3 foo4 foo5 bar1 bar2 bar3 bar4 bar5 baz1 baz2 baz3"></div>`,
  ],
});

generateRuleTests({
  name: "class-wrap",

  groupMethodBefore: beforeEach,
  groupingMethod: describe,
  testMethod: it,
  plugins: [plugin],
  config: {
    classesPerLine: 5,
  },

  good: [
    ``,
    `<div class="foo"></div>`,
    `<div class="foo md:flex p-1 bg-white rounded-lg"></div>`,
    stripIndent`
      <div class="foo1 foo2 foo3 foo4 foo5
                  pt-2 pb-2 pl-2 pr-2 mt-2
                  mb-2 ml-2 mr-2"></div>
    `,
    stripIndent`
      <div>
        <div class="foo1 foo2 foo3 foo4 foo5
                    pt-2 pb-2 pl-2 pr-2 mt-2
                    mb-2 ml-2 mr-2">
        </div>
      </div>
    `,
  ],

  bad: [
    {
      template: `<div class="pt-4 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2"></div>`,
      fixedTemplate: stripIndent`
        <div class="pt-4 pb-2 pl-2 pr-2 mt-2
                    mb-2 ml-2 mr-2"></div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute wrap is: 'pt-4 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2', but should be: 'pt-4 pb-2 pl-2 pr-2 mt-2
                      mb-2 ml-2 mr-2'
        `,
        line: 1,
        column: 5,
        isFixable: true,
        source: `<div class="pt-4 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2"></div>`,
      },
    },
    {
      template: `<div class="pt-8 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2"></div>`,
      fixedTemplate: stripIndent`
        <div class="pt-8 pb-2 pl-2 pr-2 mt-2
                    mb-2 ml-2 mr-2"></div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute wrap is: 'pt-8 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2', but should be: 'pt-8 pb-2 pl-2 pr-2 mt-2
                      mb-2 ml-2 mr-2'
        `,
        line: 1,
        column: 5,
        isFixable: true,
        source: `<div class="pt-8 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2"></div>`,
      },
    },
    {
      template: stripIndent`
        <div>
          <div class="pt-16 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2">
          </div>
        </div>
      `,
      fixedTemplate: stripIndent`
        <div>
          <div class="pt-16 pb-2 pl-2 pr-2 mt-2
                      mb-2 ml-2 mr-2">
          </div>
        </div>
      `,
      result: {
        message: stripIndent`
          HTML class attribute wrap is: 'pt-16 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2', but should be: 'pt-16 pb-2 pl-2 pr-2 mt-2
                        mb-2 ml-2 mr-2'
        `,
        line: 2,
        column: 7,
        isFixable: true,
        source: stripIndent`
          <div class="pt-16 pb-2 pl-2 pr-2 mt-2 mb-2 ml-2 mr-2">
            </div>
        `, // TODO: this is weird
      },
    },
  ],
});
